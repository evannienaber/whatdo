import * as React from 'react';
import { Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { configureStore, persistor } from './src/redux/store';
import ToDoList from './src/modules/ToDoList';
import AddToDoItemForm from './src/components/AddTodoItemForm';
import { ShareButton } from './src/components/ShareButton';

export default function App() {
  const [modalVisible, setModalVisible] = React.useState(false);

  return (
    <Provider store={configureStore}>
      <View style={styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          visible={modalVisible}
        >
          <AddToDoItemForm handleClose={() => setModalVisible(false)} />
        </Modal>
        <ShareButton />
        <TouchableOpacity
          style={styles.addButton}
          onPress={() => setModalVisible(true)}
        >
          <Text style={styles.addButtonText}>+</Text>
        </TouchableOpacity>
        <ToDoList />
      </View>
    </Provider>
  );
}

// Button color: #277cec

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 24,
    backgroundColor: '#0c1322',
  },
  addButton: {
    position: 'absolute',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 25,
    right: 25,
    height: 60,
    width: 60,
    borderRadius: 30,
    backgroundColor: '#277cec',
    zIndex: 10,
  },
  addButtonText: {
    fontSize: 50,
    color: '#fff',
    marginBottom: 5,
  },
});
