import taskReducer from '../src/redux/reducers/taskReducer';
import * as actions from '../src/redux/actions/actions';
import * as types from '../src/redux/actions/types';

describe('todos reducer', () => {
  it('should return the initial state', () => {
    expect(taskReducer(undefined, { type: types.INITIAL_STATE })).toEqual({
      currentId: 0,
      taskList: [],
    });
  });

  it('should handle ADD_TASK', () => {
    expect(
      taskReducer(
        { currentId: 0, taskList: [] },
        {
          type: types.ADD_TASK,
          data: 'Test add task',
        }
      )
    ).toEqual({
      currentId: 1,
      taskList: [
        {
          completed: false,
          key: 'TDI-0',
          name: 'Test add task',
        },
      ],
    });
  });

  it('should handle COMPLETE_TASK', () => {
    expect(
      taskReducer(
        {
          currentId: 1,
          taskList: [
            {
              completed: false,
              key: 'TDI-0',
              name: 'Test add task',
            },
          ],
        },
        {
          type: types.COMPLETE_TASK,
          key: 'TDI-0',
        }
      )
    ).toEqual({
      currentId: 1,
      taskList: [
        {
          completed: true,
          key: 'TDI-0',
          name: 'Test add task',
        },
      ],
    });
  });
});
