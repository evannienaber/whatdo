import * as actions from '../src/redux/actions/actions';
import * as types from '../src/redux/actions/types';

describe('actions', () => {
  it('should create an action to add a task', () => {
    const text = 'Add Task';
    const expectedAction = {
      type: types.ADD_TASK,
      data: text,
    };
    expect(actions.addTask(text)).toEqual(expectedAction);
  });
  it('should create an action to complete a task', () => {
    const text = 'WDI-1';
    const expectedAction = {
      type: types.COMPLETE_TASK,
      key: text,
    };
    expect(actions.completeTask(text)).toEqual(expectedAction);
  });
});
