import * as React from 'react';
import { registerRootComponent } from 'expo';
import { PersistGate } from 'redux-persist/integration/react';

import App from './App';
import { persistor } from './src/redux/store';

const WhatDo = () => (
  <PersistGate loading={null} persistor={persistor}>
    <App />
  </PersistGate>
);

registerRootComponent(WhatDo);
