import mockAsyncStorage from '@react-native-community/async-storage/jest/async-storage-mock';

jest.useFakeTimers();

jest.mock('react-native-vector-icons/Ionicons', () => 'Icon');

jest.mock('@react-native-community/async-storage', () => mockAsyncStorage);
