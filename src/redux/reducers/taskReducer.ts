import {
  ADD_TASK,
  COMPLETE_TASK,
  TaskActionType,
  TaskStateType,
} from '../actions/types';

const initialState: TaskStateType = {
  currentId: 0,
  taskList: [],
};

// typescript-eslint warning: Missing return type. The return type is inferred and therefore not needed
const taskReducer = (
  state: TaskStateType = initialState,
  action: TaskActionType
) => {
  switch (action.type) {
    case ADD_TASK:
      return {
        ...state,
        currentId: state.currentId + 1,
        taskList: state.taskList.concat({
          key: `TDI-${state.currentId}`,
          name: action.data,
          completed: false,
        }),
      };
    case COMPLETE_TASK:
      return {
        ...state,
        taskList: state.taskList.map((task) => {
          if (task.key === action.key) {
            return {
              ...task,
              completed: true,
            };
          } else {
            return { ...task };
          }
        }),
      };
    default:
      return state;
  }
};

export default taskReducer;
