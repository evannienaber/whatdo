import { createStore, combineReducers } from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import { persistStore, persistReducer } from 'redux-persist';

import taskReducer from './reducers/taskReducer';

// typescript-eslint warning: Missing return type. The return type is inferred and therefore not needed
const rootReducer = combineReducers({
  taskReducer,
});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const configureStore = createStore(persistedReducer);

// Ignore type error because of bug in redux-persist
// https://github.com/rt2zz/redux-persist/pull/1085
// @ts-ignore
const persistor = persistStore(configureStore);

export type RootState = ReturnType<typeof rootReducer>;

export { configureStore, persistor };
