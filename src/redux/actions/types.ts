export const ADD_TASK = 'ADD_TASK';
export const COMPLETE_TASK = 'COMPLETE_TASK';
export const INITIAL_STATE = '';

interface AddTaskAction {
  type: typeof ADD_TASK;
  data: string;
}

interface CompleteTaskAction {
  type: typeof COMPLETE_TASK;
  key: string;
}

interface InitialStateAction {
  type: typeof INITIAL_STATE;
}

export interface Task {
  key: string;
  name: string;
  completed: boolean;
}

export interface TaskStateType {
  currentId: number;
  taskList: Task[];
}

export type TaskActionType =
  | AddTaskAction
  | CompleteTaskAction
  | InitialStateAction;
