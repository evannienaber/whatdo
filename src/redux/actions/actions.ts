import { ADD_TASK, COMPLETE_TASK, TaskActionType } from './types';

export function addTask(task: string): TaskActionType {
  return {
    type: ADD_TASK,
    data: task,
  };
}

export function completeTask(key: string): TaskActionType {
  return {
    type: COMPLETE_TASK,
    key,
  };
}
