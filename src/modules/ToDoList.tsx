import * as React from 'react';
import { SectionList, StyleSheet, Text, View } from 'react-native';
import { useSelector } from 'react-redux';
import ToDoItem from '../components/ToDoItem';
import Header from '../components/Header';
import { RootState } from '../redux/store';
import { Task } from '../redux/actions/types';

interface DataType {
  title: string;
  data: Task[];
}

const ToDoList: React.FC = () => {
  const taskList = useSelector<RootState, RootState['taskReducer']['taskList']>(
    (state: RootState) => state.taskReducer.taskList
  );

  const pendingTasks: Task[] = taskList.filter((task) => !task.completed);
  const completedTasks: Task[] = taskList
    .filter((task) => task.completed)
    .reverse();

  const DATA: DataType[] = [
    {
      title: 'Pending Tasks',
      data: pendingTasks,
    },
    {
      title: 'Completed Tasks',
      data: completedTasks,
    },
  ];

  const renderSectionFooter = ({ section }: { section: DataType }) => {
    if (!section.data.length) {
      if (section.title === 'Pending Tasks') {
        return (
          <Text style={styles.emptyTaskText}>
            All done! Add new tasks and start being productive again.
          </Text>
        );
      } else {
        return (
          <Text style={styles.emptyTaskText}>No completed tasks yet.</Text>
        );
      }
    } else {
      return null;
    }
  };

  return (
    <View style={styles.container}>
      <Header />
      <SectionList
        sections={DATA}
        keyExtractor={(item) => item.key}
        renderItem={({ item }) => <ToDoItem task={item} />}
        renderSectionHeader={({ section: { title } }) => (
          <Text style={styles.dividerText}>{title}</Text>
        )}
        renderSectionFooter={renderSectionFooter}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  emptyTaskText: {
    color: '#fff',
    fontSize: 18,
    padding: 10,
  },
  dividerText: {
    display: 'flex',
    marginTop: 15,
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    borderBottomWidth: 1,
    borderBottomColor: '#ffffff59',
    paddingLeft: 10,
  },
});

export default ToDoList;
