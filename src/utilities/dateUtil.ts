export function getWeekDay(date?: Date): string {
  const currentDate = date || new Date();
  const weekDays: string[] = [
    'SUNDAY',
    'MONDAY',
    'TUESDAY',
    'WEDNESDAY',
    'THURSDAY',
    'FRIDAY',
    'SATURDAY',
    'SUNDAY',
  ];
  return weekDays[currentDate.getDay()];
}

export function getMonthName(date?: Date): string {
  const currentDate = date || new Date();
  const monthNames: string[] = [
    'JAN',
    'FEB',
    'MAR',
    'APR',
    'MAY',
    'JUN',
    'JUL',
    'AUG',
    'SEP',
    'OCT',
    'NOV',
    'DEC',
  ];
  return monthNames[currentDate.getMonth()];
}

export function getYear(date?: Date): string {
  const currentDate = date || new Date();
  return currentDate.getFullYear().toString();
}

export function getDayNumber(date?: Date): string {
  const currentDate = date || new Date();
  return currentDate.getDate().toString();
}
