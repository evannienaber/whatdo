import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
  getWeekDay,
  getYear,
  getMonthName,
  getDayNumber,
} from '../utilities/dateUtil';

const Header: React.FC = () => {
  return (
    <View style={styles.container}>
      <View style={styles.dateContainer}>
        <Text style={styles.dayText}>{getDayNumber()}</Text>
        <View style={styles.monthYearContainer}>
          <Text style={styles.monthText}>{getMonthName()}</Text>
          <Text style={styles.yearText}>{getYear()}</Text>
        </View>
      </View>
      <Text style={styles.rightBlock}>{getWeekDay()}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    paddingVertical: 10,
    paddingHorizontal: 25,
  },
  dateContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  monthYearContainer: {
    flex: 2,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  monthText: {
    fontWeight: 'bold',
  },
  yearText: {
    fontWeight: '100',
    opacity: 0.7,
  },
  dayText: {
    flex: 1,
    alignItems: 'center',
    fontSize: 40,
    fontWeight: 'bold',
  },
  rightBlock: {
    flex: 1,
    textAlign: 'right',
    fontWeight: 'bold',
    fontSize: 18,
  },
  textMain: {
    display: 'flex',
    color: '#fff',
  },
});

export default Header;
