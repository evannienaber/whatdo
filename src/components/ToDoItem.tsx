import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import { Task } from '../redux/actions/types';
import { useDispatch } from 'react-redux';
import { completeTask } from '../redux/actions/actions';

interface Props {
  task: Task;
}

const ToDoItem: React.FC<Props> = ({ task }) => {
  if (!task) {
    return null;
  }
  const dispatch = useDispatch();

  const onCompleteToggle = () => {
    dispatch(completeTask(task.key));
  };

  return (
    <View style={styles.container} key={task.key}>
      <Text style={styles.textMain}>{task.name}</Text>
      <CheckBox
        value={task.completed}
        onValueChange={onCompleteToggle}
        disabled={task.completed}
        tintColors={{ true: '#277cec', false: 'white' }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
  },
  textMain: {
    display: 'flex',
    color: '#fff',
    fontSize: 18,
  },
});

export default ToDoItem;
