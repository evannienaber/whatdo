import * as React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { useDispatch } from 'react-redux';
import { addTask } from '../redux/actions/actions';

interface Props {
  handleClose: () => void;
}

const AddToDoItemForm: React.FC<Props> = ({ handleClose }) => {
  const [value, setValue] = React.useState('');
  const dispatch = useDispatch();

  const onAddTask = () => {
    dispatch(addTask(value));
    handleClose();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Add new task</Text>
      <TextInput
        style={styles.input}
        value={value}
        onChangeText={(text) => setValue(text)}
      />
      <TouchableOpacity
        style={styles.acceptButton}
        disabled={!value.length}
        onPress={onAddTask}
      >
        <Text style={styles.buttonText}>Add</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.closeButton} onPress={handleClose}>
        <Text style={styles.buttonText}>Cancel</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0c1322',
  },
  titleText: {
    display: 'flex',
    marginTop: 15,
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    paddingLeft: 10,
    textAlign: 'center',
  },
  input: {
    backgroundColor: '#fff',
    margin: 15,
    height: 50,
    width: '80%',
    minWidth: 150,
    paddingLeft: 15,
    borderRadius: 7,
  },
  acceptButton: {
    width: 150,
    height: 50,
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#277cec',
  },
  closeButton: {
    width: 150,
    height: 50,
    margin: 15,
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff5833',
  },
  buttonText: {
    color: '#fff',
    fontSize: 18,
  },
});

export default AddToDoItemForm;
