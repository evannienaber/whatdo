import * as React from 'react';
import { Share, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useSelector } from 'react-redux';
import { RootState } from '../redux/store';

export const ShareButton = () => {
  const taskList = useSelector<RootState, RootState['taskReducer']['taskList']>(
    (state: RootState) => state.taskReducer.taskList
  );

  const pendingTasks: string[] = taskList
    .filter((task) => !task.completed)
    .map((task) => task.name);
  const completedTasks: string[] = taskList
    .filter((task) => task.completed)
    .reverse()
    .map((task) => task.name);

  const buildShareText = () => {
    const pendingString = pendingTasks.length
      ? pendingTasks.join('\n')
      : 'No pending tasks';
    const completedString = completedTasks.length
      ? completedTasks.join('\n')
      : 'No completed tasks';
    const shareString = `My tasks on WhatDo\n\nPending tasks:\n${pendingString}\n\nCompletedTasks:\n${completedString}`;
    Share.share({
      title: 'Shared tasks from WhatDo',
      message: shareString,
    });
  };

  return (
    <TouchableOpacity style={styles.container} onPress={buildShareText}>
      <Icon name="md-share" style={styles.icon} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 100,
    right: 25,
    height: 60,
    width: 60,
    borderRadius: 30,
    backgroundColor: '#277cec',
    zIndex: 10,
  },
  icon: {
    display: 'flex',
    color: '#fff',
    fontSize: 30,
  },
});
